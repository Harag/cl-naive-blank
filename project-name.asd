(defsystem "[project-name]"
  :description ""
  :version "2022.8.25"
  :author ""
  :licence "MIT"
  :depends-on ()
  :components ((:file "src/package")
               (:file "src/[project-name]" :depends-on ("src/package"))))

